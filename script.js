let collaps = document.getElementsByClassName("collapsible");
let i;
let content;
// 'let' and 'var' are both used for variables

for (i = 0; i < collaps.length; i++) {
  collaps[i].addEventListener("click", function(){
    this.classList.toggle("active");
    let content = this.nextElementSibling;
    // source: https://www.w3schools.com/jsref/prop_element_nextelementsibling.asp

    if (content.style.display === "block"){
      content.style.display = "none";}
    else {
    content.style.display = "block";
    }

    // if (condition) {
    //     //  block of code to be executed if the condition is true
    //   } else {
    //     //  block of code to be executed if the condition is false
    //   }
    
    // Source: https://www.w3schools.com/js/js_if_else.asp

  });
}