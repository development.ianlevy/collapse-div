# Collapse div

**Original idea**<br>
I will try to make a collapsible div element with JavaScript. I came up with
this idea myself when I noticed the amount of text on my website was getting 
too much. I will use this in my website if I successfully complete it.  

**approach**<br>
I did research on what commands I need for this to work. I found a code snippet
on w3schools that I used to start. Then i started to take appart the Javascript
to see if I understood what was happening. The HTML and CSS was not a problem, 
I would say I know pretty much all the standard stuff in CSS/HTML now.

**Learned and struggles**<br>
I learned a couple more commands in JavaScript, for example 'var' and 'let', 
'nextElementSibling' and the 'if-else statement'.

**Sources**<br>
[if-else statement](https://www.w3schools.com/js/js_if_else.asp)<br>
[nextElementSibling](https://www.w3schools.com/jsref/prop_element_nextelementsibling.asp)

Demo can be found [Here.](http://i414224.hera.fhict.nl/Development/Collapse_div/)